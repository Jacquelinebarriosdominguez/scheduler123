#include "scheduler.h"
#include <stdio.h>
#include <stdlib.h>
void sc_setID(struct Task t, int ID){
	t.id=ID;
}
int sc_getID(struct Task t){
	return t.id;
}
void sc_agregarTareas(struct Task t, int index){
	Tareas[index]=t;
}
void sc_ejecutarTareas(){
	main(){
		struct Task tarea1,tarea2;
		sc_setID(tarea1,100);
		int id=sc_getID(tarea1);
		sc_agregarTarea(tarea1,0);
		sc_agregarTarea(tarea2,1);
		for(int i=0; i<10; i++){
			sc_agregarTarea(tarea++,i);
		}
	}
}